# Changelog

## 0.0.11.dev2 (Unreleased)

-   Show frequency resolution and number of FFT averages in post-processing
    plots.
-   Fixed wrong units in coupling function plot labels
-   Added DARM glitch gating option (zeroes out time series when BLRMS
    exceeds median by factor of 2).
-   Moved plotting methods to new plotting submodule.
-   Added interactive mode when no arguments are passed to pemcoupling.

## 0.0.10 (2019-06-17)

-   Renamed Sensor -> PEMChannel; SensorTS -> PEMTimeSeries;
    SensorASD -> PEMFrequencySeries.
-   Reorganized DARM fetching to be parallel sensor fetching.
-   Vectorized CoupFunc.compute() main calculation.
-   Fixed incorrect magnetometer calibration factors.
-   Use 'Measured' instead of 'Real' flags.
    -   Renamed all 'real' variables to 'measured'.
-   Added ignore_saturated as a cmd-line arg and config option.
-   Revamped configuration format.
    -   Consolidated configuration and channel lists into
        a single config.ini file with different channel categories
        separated into sections.
    -   Sections are passed as positional args after config file and looped
        over in the main pemcoupling routine.
    -   Updated documentation.
    -   Removed obsolete ifo argument.
-   Grouped command-line arguments for a clearer help menu.
-   Generate HTML reports for individual injections and composite
    coupling functions.

## 0.0.9 (2019-05-05)

-   Bug fixes to how quad sum channels are handled.
-   Removed upper-limit truncation in composite_utils.
-   Small tweaks to sitewide plotting.
-   New method of composite coupling functions for quad sum channels.
    -   Take max sensor ratio of all axes instead of just quad sum.
-   Simplified channel and injection searching by making a single search
    function in utils.
-   Merged get_composite_coup_func into composite.py
-   New model for storing channel/calibration data:
    -   Combined channel list and calibration table into a single file
        that will be used as both a channel list and calibration table.
    -   PEM_calibration.csv is now obsolete.
-   Include comb frequency in header of .txt output files.
    -   This is used by composite and sitewide scripts for comb injections.
-   Ignore zeros when looking for reference times in DTTs.
-   Implemented autoscaled sensor thresholds.
    -   Sensor threshold can now be above the given value. It is determined
        as a ratio of the loudest sensor amplitude for the same injection.
    -   Quad sum channels are treated separately from regular channels.
-   Aesthetic changes to composite and sitewide plots.
-   New config reference file.

## 0.0.8 (2019-03-26)

-   Improved condor handling of DTTs and injection lists.
    -   `--condor` now creates temporary single-injection lists to feed into
        each `pemcoupling` job.
-   More config options trimming.
-   Added `--freq-range`, `--cf-range`, and `--amb-range` to
    `pemcoupling-composite`.
-   Aesthetic changes to composite and sitewide plotting.
-   Added `--summary` option to `pemcoupling` and `pemcoupling-composite` to
    generate summary plots after composite plots finish.
-   The way `pemcoupling` calls on `pemcoupling.composite` and
    `pemcoupling.sitewide` is now more streamlined.

## 0.0.7 (2019-03-25)

-   Fixed a GWINC loading bug. Forgot to import numpy...

## 0.0.6 (2019-03-25)

-   Overhaul of injection times parsing.
    -   New function for getting DTT times using `xml` module, more robust to
        weird ordering of references and measurements.
    -   Better error handling.
-   Moved DARM notch lists into main config files.
    -   Old darmnotchL*O.txt files removed.
-   Converted GWINC file to two-column gwinc.txt file instead of matlab format.
    -   Removed `loaddata.get_gwinc` function.
-   Updated LHO calibration file in config_files.
-   Better handling of plot axis limits.
    -   No more errors with custom plot limits via command-line args or config
        options.
    -   Default axis limits are more reasonable now, plots are more readable.
-   Fixed bug that excluded auto-generated quad-summed channels from being
    smoothed.
-   Removed summary plot channel clustering for plots with < 15 channels.
-   Added FFT arguments to `pemcoupling` and re-organized FFT parameter
    parsing.
-   Now only filter saturated channels if `auto_freq_bands` is off.
-   Debugged `condor.py` handling of arguments.
-   Moved function for frequency line searching to `utils`.

## 0.0.5 (2019-03-17)

-   Made improvements to ratio plots.

## 0.0.4 (2019-03-14)

-   Added sphinx and sphinxcontrib-programoutput to requirements

## 0.0.3 (2019-03-12)

-   Improvements to `pemcoupling.injection.Injection`.
    -   "name" is now an optional argument.
    -   Restructured DARM fetching/calibrating methods to parallel sensor
        fetching/calibrating methods.
-   Fixed a bug in how composite coupling functions are calculated.
    This bug was causing some missing values due to some indexing errors.
-   Updated docs to accomodate changes to command-line interface and to
    include documentation for `Injection` and `CoupFunc` object classes.   

## 0.0.2 (2019-03-07)

-   Created an Injection object to simplify the workflow for analyzing a single
    injection. This object makes it easy to write your own script for analyzing
    injections, e.g.
    ```python
    from pemcoupling.injection import Injection
    t_background = 1234567000
    t_injection = 1234568000
    duration = 60
    injection = Injection("injection_name", t_background, t_injection, duration)
    ```

-   Modified argument and configuration parsing to be more user friendly and
    easier to debug. The command-line arguments `ifo`, `station`, and
    `injection-type` are no longer required if channels are being manually
    provided. They are instead inferred from the given channels. Now the only
    required argument is the config directory.
    -   `pemcoupling-sitewide` now has many more command-line arguments for
        for customized plotting.
    -   Some changes were made to the config files to accomodate the above.

-   The code was revised to follow PEP-8 conventions more strictly.

## 0.0.1 (2019-01-10)

-   Initial version.
