.. _injection:

The Injection
=============

The ``Injection`` object is used to analyze injections. It houses a
number of methods for fetching and pre-processing the necessary
spectra, and a ``get_coupling_functions`` method that outputs
``CoupFunc`` objects.

Create an ``Injection`` object: ::

    from pemcoupling.injection import Injection

    name = "my_injection"
    t_bkg = 1234567890  # background time (GPS)
    t_inj = 1234569870  # injection time (GPS)
    duration = 60       # seconds
    fftlength = 10      # seconds
    overlap = 5         # seconds
    channels = ['channel_1', 'channel_2']
    smooth_dict = {
        'channel_1': (5, 0.5, True)
    }

    injection = Injection(
        t_bkg,
        t_inj,
        duration,
        name=name,
        channels=channels,
        fftlength=fftlength,
        overlap=5
        smooth_dict=smooth_dict
    )

Fetch spectra for sensors: ::

    injection.get_sensor_ASDs()

Calibrate your sensors with an existing calibration file: ::
    
    from pemcoupling.preprocess import get_sensor_calibration

    calibration_file = "PEM_calibration.csv"
    calibration_factors = get_sensor_calibration(channels, calibration_file)
    injection.calibrate_sensors(calibration_factors)

Or use your own dictionary of calibration factors: ::
    
    calibration_factors = {
        'channel_1': 6.1e-6,
        'channel_2': 5.6e-6
    }
    injection.calibrate_sensors(calibration_factors)

Fetch spectra for DARM: ::

    injection.get_DARM_ASDs(gw_channel='darm')

Calibrate DARM from a calibration file: ::

    from pemcoupling.preprocess import get_DARM_calibration

    calibration_file = "DARM_calibration.csv"
    darm_cal_freqs, darm_cal_factors = get_DARM_calibration(calibration_file)
    injection.calibration_DARMs(darm_cal_freqs, darm_cal_factors)

Get coupling functions: ::
    
    darm_threshold, sensor_threshold = (2, 2)  # Default values
    smooth_dict = {'smoothing': (5.0, 0.5), 'smoothing_log': True}
    coupling_functions_list = injection.get_coupling_functions(
        darm_threshold=darm_threshold,
        sensor_threshold=sensor_threshold,
        smooth_dict=smooth_dict
    )

Create plots or CSV outputs: ::

    for cf in coupling_functions_list:
        cf.plot(cf.name + '_coupling.png')
        cf.specplot(cf.name + '_spectra.png')
        cf.to_csv(cf.name + '.txt')
