.. _how-preprocess:

Pre-processing
==============

Calibration
-----------

Calibration is applied using a data table (provided in the config files).
Accelerometer calibration is usually given in m/s^2 and is converted to displacement.


.. _smoothing:

Smoothing
---------

Spectra can be smoothed using a linear or logarithmic sliding average.
This suppresses bin-to-bin fluctuations that result in noisy coupling functions.
Additional smoothing can be applied to the injection spectra of microphones.
This is to deal with peaks and troughs in their spectra resulting from a microphone
lying at the node of acoustic injections.