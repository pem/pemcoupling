.. _usage-config:

Configuring pemcoupling
=======================

Running `pemcoupling <pemcoupling>`_, `pemcoupling-composite <composite>`_,
and `pemcoupling-sitewide <sitewide>`_ always requires pointing to the desired
configuration file. A default config file is provided in the source code as
``config_files/config-lho.ini`` and ``config_files/config-llo.ini`` for LHO and
LLO, respectively. These can be copied and edited to your liking.

The ``[darm]`` section handles the choice of the GW channel and its calibration.
Calibration files ``config_files/darmcalLHO.txt`` and ``config_files/darmcalLLO.txt``
are provided, but make sure they are linked to properly from the location of
``config.ini``.

The analysis portion of the configuration file is split into sections. In the
above example, the ``[MAG]`` section covers analysis options and channel info for
magnetometer channels, the ``[ACC]`` section for accelerometers, and so on. You
can make your own sections or modify the existing ones.

.. raw:: html

    The <a href="../_static/config-ref.ini">reference configuration file</a>
    explains each config option found in these files.
        