#### CONFIG FILE FOR PEMCOUPLING ####

[darm]
#   GW channel without the ifo prefix
gw_channel = L1:CAL-DELTAL_EXTERNAL
#   DARM calibration ASCII file
calibration = darmcalLLO.txt
#   Frequencies to omit when computing coupling functions
notches =
    15 17.5
    59.5 60.5
    179 181
    305 309
    314.3 316
    329 333
    433 436
    497 520
    798 802
    990 1027
    1450 1560
    1940 2000

#   SENSOR_TYPE = {MAG, ACC, MIC, WFS, etc.}
#   These channels will be processed with these settings.
[SENSOR_TYPE]
#   Spectrum smoothing parameters given as the width (in Hz)
#   of the smoothing window. If log-smoothing, these are the
#   window widths at freq=100Hz. If truee value is given, it
#   will be treated as the smoothing for all ASDs. If two
#   values are given, the first will apply to all GW channel
#   ASDs and to background sensor ASDs. The second will apply
#   to injection sensor ASDs.
#   Prefix the option name with a desired sensor type, e.g.
#   "ACC_smoothing: 0.5". To apply the same smoothing to all
#   sensor, omit the prefix, e.g. "smoothing: 0.5".
smoothing = 0.2 0.2
#   Scale smoothing window proportionally with frequency.
smoothing_log = true
#   Duration of all time series (in seconds).
#   Required if band_width is not provided.
duration =
#   Number of FFT averages for computing ASDs.
fft_avg = 10
#   Percentage overlap of FFT segments.
overlap_pct = 0.5
#   Frequency resolution (1 / FFT time) (in Hz).
#   Required if duration is not provided.
band_width = 0.03
#   Frequency range (in Hz) for analysis.
#   ASDs will be cropped before analysis steps.
freq_min = 5
freq_max = 2048
#   If True, saturated channels (exceeding 32k in their time series amplitudes)
#   are skipped in the analysis.
ignore_saturated = true
#   Enable quadrature-summing of multi-axis sensors.
#   A new quad-sum channel is created from the ASDs
#   of the individual axes. Magnetometer quad sum
#   channel names will end with '_XYZ'. WFS quad sum
#   channel names will end with '_PITWAY_QUADSUM'.
quad_sum = true
#   Thresholds for distinguishing measurements vs. upper limits.
#   The thresholds are a minimum ratio of injection ASD to background ASD.
#   If only sensor ASD exceeds sens_factor_threshold, then the value is an upper limit.
#   If both sensor ASD exceeds sens_factor_threshold and DARM ASD exceeds darm_factor_threshold,
#   then the value is a measured value.
#   Thresholds for classifying coupling factors based on the
#   ratios of injection ASDs to background ASDs.
#   Sensor threshold determines where coupling can be calculated.
#   DARM threshold determines whether those coupling factors are
#   upper limits or measurements (real values).
darm_threshold = 2
sensor_threshold = 5
#   If given, sensor thresholds will be scaled proportionally to
#   the highest sensor ratio seen in all channels. This allows
#   the threshold to reflect the actual strength of the injection.
#   The sensor_threshold is still applied, and hence can be
#   thought of as a minimum value for the scaled threshold.
sensor_threshold_autoscale =
#   Only measure coupling between these frequencyes (Hz)
broadband_freqs =
#   Only measure coupling near multiples of comb_freq (Hz)
comb_freq =
#   Only measure coupling near line_freq (Hz)
line_freq =
#   Search only for local maxima within this window size (in Hz).
#   Used for making sure comb and line injections only yield
#   one coupling factor at each injected line.
peak_search_window = 0.2
#   Automatically determine frequency band based on injection name.
auto_broadband_freqs = true
auto_comb_freqs = true
#   List of channel names and calibration data
#   If no calibration data given, channel ASD and coupling function
#   will not be calibrated.
channels =
    L1:PEM-CS_MAG_EBAY_ISCRACK_X 610e-12 T
    L1:PEM-CS_MAG_EBAY_ISCRACK_Y 610e-12 T
    L1:PEM-CS_MAG_EBAY_ISCRACK_Z 610e-12 T


#### EXPORT OPTIONS ####

[coupling function plot]
#   Figure width/height of coupling function plot.
coup_fig_width = 18
coup_fig_height = 9
#   Figure width/height of spectrum plot.
spec_fig_width = 18
spec_fig_height = 9
#   Show DARM / 10 line for reference.
darm/10 = true
#   Frequency (X-axis) plot limits.
#   This is for visuals only; exported data will still follow
#   freq_min and freq_max given in [asd]
freq_min =
freq_max =
#   Coupling function (Y-axis) plot limits.
coup_min =
coup_max =
#   Estimated ambient (Y-axis) plot limits.
amb_min =
amb_max =

#   Each plot option below takes two values: the first is applied to
#   measured values and the second is applied to upper limits.
#   Marker styles. Options are {o, '^', 'x', '.'}
markers = o o
#   Marker sizes
markersizes = 3 3
#   Marker edge widths
edgewidths = 0 0
#   Marker face color (choose from matplotlib "named colors")
facecolors = dodgerblue grey
#   Marker edge color (choose from matplotlib "named colors")
edgecolors = none none


[ratio plot]
#   Plot injection-to-background ASD ratios for all channels.
plot = false
#   Frequency (X-axis) plot limits.
freq_min =
freq_max =
#   Ratio (Z-axis) plot limits.
ratio_min =
ratio_max =
#   Method for binning ASD ratios {max, avg, raw}.
method = max
#   Maximum number of channels per ASD ratio plot.
channels_per_plot = 10


#### COMPOSITE COUPLING FUNCTIONS ####

[composite coupling function]
#   Default name of the directory for saving/loading
#   composite coupling functions.
composite_dir = CompositeCouplingFunctions
#   Default name of the directory for saving summary plots.
summary_dir = SummaryPlots
#   Plot DARM ASD as DARM or convert to STRAIN
gw_signal = darm
#   Filename of GWINC noise curve.
gwinc_file = gwinc.txt
#   Label for GWINC noise curve.
gwinc_label = GWINC, 125 W, No Squeezing
#   Figure width/height for all composite plots.
fig_width = 18
fig_height = 12
#   Frequency (X-axis) plot limits.
freq_min = 5
freq_max = 2048
#   Coupling function (Y-axis) plot limits.
coup_min =
coup_max =
#   Estimated ambient (Y-axis) plot limits.
amb_min =
amb_max =

#   Each plot option takes two values: the first is applied to
#   measured values and the second is applied to upper limits.
#   Marker styles {o, '^', 'x', '.'}
markers = o o
#   Marker sizes
markersizes = 3 3
#   Marker edge widths
edgewidths = 0 0
#   Marker face color (choose from matplotlib "named colors")
facecolors = red blue
#   Marker edge color (choose from matplotlib "named colors")
edgecolors = none none
#   Multi-plot marker styles {o, '^', 'x', '.'}
split_markers = o ^
#   Multi-plot marker size
split_markersizes = 6 3
#   Multi-plot marker edge widths
split_edgewidths = 0.2 0.5
