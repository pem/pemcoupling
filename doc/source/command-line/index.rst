.. _usage:

Command-line tools
==================

The ``pemcoupling`` package provides tools for computing coupling functions and more.

Contents:

.. toctree::
   :maxdepth: 2

   pemcoupling
   composite
   sitewide
   config
   report
