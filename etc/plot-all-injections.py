#! /usr/bin/python

import sys
import os
import glob
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.patches as mpatches

MULTIPLOT_COLORMAP = 'gist_rainbow'

def get_colors(labels, colormap=MULTIPLOT_COLORMAP):
    """
    Gives a color to each label chosen from colormap. Use default
    matplotlib color cycle if there are only 5 or fewer colors.
    """

    if len(labels) > 5:
        cmap = plt.get_cmap(colormap)
        colors = cmap(np.linspace(0, 0.95, len(labels)))
    else:
        color_cycle = ['b', 'r', 'g', 'm', 'c']
        colors = [mcolors.ColorConverter().to_rgba(c)
                  for c in color_cycle]
    colors_dict = {x: colors[i] for i, x in enumerate(labels)}
    return colors_dict

# channels = [
#     'H1:PEM-CS_ACC_BEAMTUBE_SRTUBE_X',
#     'H1:PEM-CS_ACC_HAM5_SRM_X',
#     'H1:PEM-CS_ACC_HAM5_SRM_Y',
#     'H1:PEM-CS_ACC_HAM6VAC_SEPTUM_Y',
#     'H1:PEM-CS_ACC_HAM6_OMC_X',
#     'H1:PEM-CS_ACC_HAM6_OMC_Y',
#     'H1:PEM-CS_ACC_HAM6_OMC_Z'
# ]
channels = [
    'H1:PEM-CS_MAG_EBAY_LSCRACK_XYZ',
    'H1:PEM-CS_MAG_EBAY_SUSRACK_XYZ'
]

try:
    directory = sys.argv[1]
except IndexError:
    directory = os.getcwd()
for channel in channels:
    pattern = os.path.join(directory, '*', '{}_coupling_data.txt'.format(channel))
    files = glob.glob(pattern)
    if not files:
        print("No coupling function data found for channel {}".format(channel))
        continue
    df_dict = {os.path.basename(os.path.dirname(f)): pd.read_csv(f, comment='#') for f in files}
    df_dict = {injection: df for injection, df in df_dict.items() if 'Real' in df['flag'].values}
    injections = sorted(df_dict.keys())
    colors = get_colors(injections)

    darm_freqs = df_dict.values()[0]['frequency']
    darm_values = pd.concat([df['darmBG'] for df in df_dict.values()], axis=1).min(axis=1)

    fig, ax = plt.subplots(1, 1, figsize=(18, 12))
    ax.set_position([0.1, 0.1, 0.98 - 0.1, 0.9 - 0.1])
    ax.plot(darm_freqs, darm_values, '-', color='0.0')
    ax.plot(darm_freqs, darm_values / 10, '--', color='0.6')
    patches = [mpatches.Patch(fc='0.0')]
    labels = ['DARM background']
    amb_min = min(darm_values)
    amb_max = min(darm_values)
    for injection in injections:
        df = df_dict[injection]
        color = colors[injection]
        real = df[df['flag'] == 'Real']
        freqs = real['frequency']
        ambients = real['factor'] * real['sensBG']
        ax.plot(freqs, ambients, 'o', ms=7, c=color)
        patches.append(mpatches.Patch(fc=color))
        labels.append(injection)
        if min(ambients) < amb_min:
            amb_min = 10 ** np.floor(np.log10(min(ambients)))
        if max(ambients) > amb_max:
            amb_max = 10 ** np.ceil(np.log10(max(ambients)))
    ax.set_xscale('log', nonpositive='clip')
    ax.set_yscale('log', nonpositive='clip')
    #ax.set_xticks(range(0, 200, 2))
    ax.set_xlim(5, 3000)
    ax.set_ylim(1e-24, 1e-16)
    ax.set_title("{} (Quadrature sum of X, Y, and Z components)\nEstimated ambients from all injections"
                 .format(channel.replace('_', ' ').replace('_DQ', '')), size=30 * 0.85)
    ax.set_xlabel("Frequency [Hz]", size=25)
    ax.set_ylabel("DARM ASD [m/sqrt(Hz)]", size=25)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(25)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(25)
    plt.grid(axis='both', which='major', ls=':', color='0.0')
    plt.grid(axis='both', which='minor', ls=':', color='0.6')
    plt.legend(patches, labels, fontsize=15)

    #plt.show()
    fig.savefig('{}_all_injections.png'.format(channel))
