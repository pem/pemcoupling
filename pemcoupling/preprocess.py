"""
Preprocessing tools for PEM coupling calculations.
"""

import numpy as np
import logging
import re

# pemcoupling modules
from .pemchannel import PEMChannel, PEMFrequencySeries
from .utils import quad_sum_names


def reject_saturated(time_series_list, verbose=False):
    """
    Reject saturated sensors based on whether the time series exceeds 32000
    ADC counts.

    Parameters
    ----------
    time_series_list : list
        TimeSeries objects.
    verbose : {False, True}, optional
        If True, report saturated channels.

    Returns
    -------
    time_series_unsaturated : list
        TimeSeries objects that did not exceed the saturation threhold.
    """

    bad_channels = []
    time_series_unsaturated = []
    for time_series in time_series_list:
        name = time_series.name
        if (('ACC' in name or 'ADC' in name)
                and (time_series.value.max() >= 32000)):
            bad_channels.append(name)
            logging.info('Channel rejected due to saturation: {}'.format(name))
        else:
            time_series_unsaturated.append(time_series)
    # Report results
    num_saturated = len(bad_channels)
    num_channels = len(time_series_list)
    if (num_saturated > 0):
        print('{}/{} channels rejected due to signal saturation'
              .format(num_saturated, num_channels))
        if verbose:
            print('The following ACC channels were rejected: ')
            for bad_chan in bad_channels:
                print(bad_chan)
    return time_series_unsaturated


def get_DARM_calibration(calibration_file):
    if calibration_file is not None:
        try:
            data = np.loadtxt(calibration_file)
        except (IOError, OSError):
            err_msg = ("Calibration file {} failed to load."
                       .format(calibration_file))
            logging.error(err_msg)
            raise IOError(err_msg)
        # Get frequencies and calibration factors
        freqs = data[:, 0]
        dB_ratios = data[:, 1]
        factors = 10.0 ** (dB_ratios / 20.)
    else:
        freqs, factors = np.array([]), np.array([])
    return freqs, factors


def quad_sum_ASD(asd_list, replace_original=False):
    """
    Create a quadrature sum ASD of multi-axial sensors (i.e. magnetometers).

    Parameters
    ----------
    asd_list : list
        pemchannel.PEMFrequencySeries objects with '_X', '_Y', or '_Z' in
        their names.
    replace_original : bool
        If True, remove original single-component channels from ASD list.

    Returns
    -------
    asd_list_qsum : list
        Quadrature-summed pemchannel.PEMFrequencySeries object(s).
    """

    channel_names = [asd.channel.name.replace('_DQ', '') for asd in asd_list]
    qsum_dict = quad_sum_names(channel_names)
    asd_list_qsum = []
    for name, axes in qsum_dict.items():
        qsum_channel = PEMChannel(name)
        asd_axes = [
            asd for asd in asd_list if
            asd.channel.name.replace('_DQ', '') in axes]
        logging.info("Generating quad sum ASD for " + name)
        qsum_values = np.sqrt(sum([asd.values**2 for asd in asd_axes]))
        if asd_axes[0].calibration is None:
            calibration = None
        elif re.search('PEM-.*_MAG_.*_XYZ', name) is not None:
            calibration = np.sqrt(3) * asd_axes[0].calibration
        elif re.search('IMC-WFS_.*PITYAW', name) is not None:
            if all([axis.calibration is not None for axis in asd_axes]):
                calibration = np.sqrt(
                    asd_axes[0].calibration**2 + asd_axes[1].calibration**2)
            else:
                calibration = None
        else:
            calibration = None
        qsum = PEMFrequencySeries(
            name, asd_axes[0].freqs, qsum_values, t0=asd_axes[0].t0,
            channel=qsum_channel, unit=asd_axes[0].unit,
            calibration=calibration, n_avg=asd_axes[0].n_avg)
        asd_list_qsum.append(qsum)
    return asd_list_qsum


def smooth_ASD(x, y, width, smoothing_log=False):
    """
    Sliding-average smoothing function for cleaning up noisiness in a spectrum.
    Example of logarithmic smoothing:
    If width = 5 and smoothing_log = True, smoothing windows are defined such
    that the window is 5 frequency bins wide (i.e. 5 Hz wide if bin width = 1
    Hz) at 100 Hz, and 50 bins at 1000 Hz.
    ...
    A bit about smoothing: the underlying motivation is to minimize random
    noise which artificially yields coupling factors in the calculations later,
    but smoothing is also crucial in eliminating point-source features (e.g.
    drops in microphone spectrum due to the point-like microphone sitting at an
    anti-node of the injected sound waves). It is not so justifiable to smooth
    the sensor background, or DARM, since background noise and overall coupling
    to DARM are diffuse, so smoothing of these should be very limited.

    Parameters
    ----------
    x : array
        Frequencies.
    y : array
        ASD values to be smoothed.
    width : int
        Size of window for sliding average (measured in frequency bins).
    smoothing_log : {False, True}, optional
        If True, smoothing window width grows proportional to frequency
        (see example above).

    Returns
    -------
    y_smooth : array
        Smoothed ASD values.
    """

    y_smooth = np.zeros_like(y)
    if smoothing_log:
        # Num of bins at frequency f:  ~ width * f / 100
        widths = np.round(x * width / 100).astype(int)
        for i, w in enumerate(widths):
            lower_ = max([0, i - int(w / 2)])
            upper_ = min([len(y), i + w - int(w / 2) + 1])
            y_smooth[i] = np.mean(y[lower_:upper_])
    else:
        for i in range(len(y)):
            lower_ = max([0, i - int(width / 2)])
            upper_ = min([len(y), i + width - int(width / 2)])
            y_smooth[i] = np.mean(y[lower_:upper_])
    return y_smooth
