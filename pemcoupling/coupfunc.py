import numpy as np
import pandas as pd
import re
import logging

# pemcoupling modules
from .pemchannel import PEMFrequencySeries
from .preprocess import smooth_ASD
from . import utils as pem_utils


COLUMNS = [
    'frequency',
    'factor',
    'factor_counts',
    'flag',
    'sensINJ',
    'sensBG',
    'darmINJ',
    'darmBG'
]
CSV_FORMAT = '%.2f,%.2e,%.2e,%s,%.2e,%.2e,%.2e,%.2e'


class CoupFunc(PEMFrequencySeries):
    """
    Composite coupling function data for a single sensor

    Attributes
    ----------
    name : str
        Name of PEM sensor.
    freqs : array
        Frequency array.
    values : array
        Coupling factors in physical units.
    values_in_counts : array
        Coupling factors in raw counts.
    flags : array
        Coupling factor flags
        ('Measured', 'Upper limit', 'Thresholds not met', 'No data').
    sens_bkg : array
        ASD of sensor background.
    sens_inj : array
        ASD of sensor injection.
    darm_bkg : array
        ASD of DARM background.
    darm_inj : array
        ASD of DARM injection.
    ambients : array
        Estimated ambient ASD for this sensor.
    df : float
        Frequency bin width in Hz
    calibration : float
        Calibration factor, used to convert back to raw counts if counts
        coupling function not provided.
    unit : str
        Sensor unit of measurement
    qty : str
        Quantity measured by sensor
    coupling : str
        Coupling type associated with sensor.

    Methods
    -------
    load
        Load coupling function from csv/txt file
    compute
        Compute coupling function from PEM and DARM ASDs
    plot
        Plot coupling function from the data.
    ambientplot
        Plot DARM spectrum with estimated ambient of the sensor.
    to_csv
        Save data to csv file.
    """

    def __init__(
            self, name, freqs, values, flags, sens_bkg, darm_bkg,
            sens_inj=None, darm_inj=None, t_bg=0, t_inj=0,
            injection_name=None, unit='', calibration=None, n_avg=1,
            values_in_counts=None, comb_freq=None, line_freq=None):
        super(CoupFunc, self).__init__(
            name, freqs, values, unit=unit, calibration=calibration, n_avg=n_avg)
        self.flags = np.asarray(flags, dtype=object)
        self.sens_bkg = np.asarray(sens_bkg)
        self.sens_inj = np.asarray(sens_inj)
        self.darm_bkg = np.asarray(darm_bkg)
        self.darm_inj = np.asarray(darm_inj)
        self.ambients = self.values * self.sens_bkg
        self.t_bg = int(t_bg)
        self.t_inj = int(t_inj)
        self.injection_name = injection_name
        if values_in_counts is not None:
            self._values_in_counts = np.asarray(values_in_counts)
        else:
            self._values_in_counts = None
        self.comb_freq = comb_freq
        self.line_freq = line_freq

    @property
    def values_in_counts(self):
        if self._values_in_counts is not None:
            return self._values_in_counts
        elif self.calibration is not None:
            return self.values * self.calibration
        else:
            return self.values

    @classmethod
    def load(cls, filename, channel=None):
        """
        Loads csv coupling function data into a CoupFunc object.

        Parameters
        ----------
        filename : str
            Name of coupling function data file.

        Returns
        -------
        cf : couplingfunction.CoupFunc object
        """
        try:
            comb_freq = None
            line_freq = None
            with open(filename, 'r') as f:
                for line in f:
                    if '#' in line:
                        comb_match = re.search('#combfreq:([\d.]+)', line)
                        line_match = re.search('#linefreq:([\d.]+)', line)
                        if comb_match:
                            comb_freq = float(comb_match.group(1))
                        elif line_match:
                            line_freq = float(line_match.group(1))
            data = pd.read_csv(filename, comment='#', delimiter=',')
        except (IOError, OSError):
            print('')
            logging.warning('Invalid file or file format: ' + filename)
            print('')
        cf = cls(
            channel, data.frequency, data.factor, data.flag,
            data.sensBG, data.darmBG, sens_inj=data.sensINJ,
            values_in_counts=data.factor_counts, comb_freq=comb_freq,
            line_freq=line_freq)
        return cf

    @classmethod
    def compute(
            cls, sens_bkg, sens_inj, darm_bkg, darm_inj,
            darm_threshold=2, sensor_threshold=2, scaled_sensor_threshold=2,
            injection_type='broadband',
            peak_search_window=0, smooth_params=(0, 0), smooth_log=False,
            notch_windows=[], broadband_freqs=None, comb_freq=None,
            line_freq=None, injection_name=None, slope_threshold=None,
            verbose=False):
        """
        Calculates coupling factors from sensor spectra and DARM spectra.

        Parameters
        ----------
        sens_bkg : ChannelASD object
            ASD of PEM sensor during background.
        sens_inj : ChannelASD object
            ASD of PEM sensor during injection.
        darm_bkg : ChannelASD object
            ASD of DARM during background.
        darm_inj : ChannelASD object
            ASD of DARM during injection.
        darm_threshold : float, int, optional
            Coupling factor threshold for determining measured coupling
            factors vs upper limits. Defaults to 2.
        sensor_threshold : float, int, optional
            Coupling factor threshold for determining upper limit vs no
            injection. Defaults to 2.
        peak_search_window : float, int, optional
            Width of local max restriction. E.g. if 2, keep only coupling
            factors that are maxima within +/- 2 Hz.
        smooth_params : tuple, optional
            Injection smooth parameter, background smoothing parameter,
            and logarithmic smoothing.
        notch_windows : list, optional
            List of notch window frequency pairs (freq min, freq max).
        verbose : {False, True}, optional
            Print progress.

        Returns
        -------
        cf : CoupFunc object
            Contains sensor name, frequencies, coupling factors, flags
            ('Measured', 'Upper Limit', 'Thresholds not met', or 'No data'),
            and other information.
        """

        # Gather relevant data
        name = sens_bkg.channel.name
        unit = str(sens_bkg.unit)
        calibration = sens_bkg.calibration
        n_avg = sens_bkg.n_avg
        t_bg = sens_bkg.t0
        t_inj = sens_inj.t0
        freqs = sens_inj.freqs
        nfreqs = len(freqs)
        bin_width = sens_bkg.df
        sens_bkg_asd = sens_bkg.values
        sens_inj_asd = sens_inj.values
        darm_bkg_asd = darm_bkg.values
        darm_inj_asd = darm_inj.values
        # Crop DARM if longer than sensor
        darm_bkg_asd = darm_bkg_asd[:nfreqs]
        darm_inj_asd = darm_inj_asd[:nfreqs]
        # "Reference background": this is smoothed w/ same smoothing as
        # injection spectrum. Thresholds use both sens_bkg and sens_bkg_ref
        # for classifying coupling factors.
        sens_bkg_ref_asd = np.copy(sens_bkg_asd)
        # Cast thresholds as arrays if int/float given
        if isinstance(sensor_threshold, (int, float)):
            sensor_threshold = np.ones(nfreqs) * sensor_threshold
        if isinstance(scaled_sensor_threshold, (int, float)):
            scaled_sensor_threshold = np.ones(nfreqs) * scaled_sensor_threshold
        if isinstance(darm_threshold, (int, float)):
            darm_threshold = np.ones(nfreqs) * darm_threshold
        # Crop scaled sensor threshold if longer than sensor
        scaled_sensor_threshold = scaled_sensor_threshold[:nfreqs]
        # OUTPUT ARRAYS
        factors = np.zeros(nfreqs)
        flags = np.array(['No data'] * nfreqs, dtype=object)
        # SMOOTH SPECTRA
        if any(x > 0 for x in smooth_params):
            smooth_base = int(smooth_params[0] / bin_width)
            if len(smooth_params) == 2:
                smooth_inj = int(smooth_params[1] / bin_width)
            else:
                smooth_inj = smooth_base
            sens_bkg_ref_asd = smooth_ASD(
                freqs, sens_bkg_asd, smooth_inj, smooth_log)
            sens_bkg_asd = smooth_ASD(
                freqs, sens_bkg_asd, smooth_base, smooth_log)
            darm_bkg_asd = smooth_ASD(
                freqs, darm_bkg_asd, smooth_base, smooth_log)
            sens_inj_asd = smooth_ASD(
                freqs, sens_inj_asd, smooth_inj, smooth_log)
            darm_inj_asd = smooth_ASD(
                freqs, darm_inj_asd, smooth_base, smooth_log)
        #######################################################################
        # DETERMINE FREQUENCY BINS TO ANALYZE
        # Keep only freqs that are within (0.5 * peak_search_window) of the
        # specified freqs given by comb_freq or line_freq
        if (
                (injection_type == 'comb') and
                (comb_freq is not None) and
                (peak_search_window > 0)
        ):
            # Get indices near harmonics of comb frequency
            freq_mask = pem_utils.near_comb(
                comb_freq, freqs, peak_search_window)
        elif (
                (injection_type == 'line') and
                (line_freq is not None) and
                (peak_search_window > 0)
        ):
            # Get indices near line frequencies
            freq_mask = pem_utils.near_line(
                line_freq, freqs, peak_search_window)
        elif (injection_type == 'broadband') and broadband_freqs:
            # Get indices between min and max broadband frequencies
            freq_mask = pem_utils.in_freq_band(broadband_freqs, freqs)
        else:
            freq_mask = np.ones(nfreqs, dtype=bool)
        # Add notch windows for microphone 60 Hz resonances
        notch_windows += [
            (f - 2., f + 2.) for f in range(120, int(max(freqs)), 60)]
        # Apply notch windows to loop range
        if len(notch_windows) > 0:
            # Skip freqs that lie b/w any pair of freqs in notch_windows
            notch_mask = np.ones(nfreqs, dtype=bool)
            for (lo, hi) in notch_windows:
                notch_mask[(lo < freqs) & (freqs < hi)] = False
            freq_mask = (freq_mask & notch_mask)
        # Slope threshold
        if slope_threshold is not None:
            slope_window = 0.03
            inj_slope = pem_utils.asd_slope(sens_inj_asd, window=slope_window)
            inj_slope_below_thresh = (inj_slope < slope_threshold) & (inj_slope > 1.0 / slope_threshold)
            freq_mask = (freq_mask & inj_slope_below_thresh)
        #######################################################################
        # COMPUTE COUPLING FACTORS WHERE APPLICABLE
        # sens_ratio = sens_inj / np.maximum(sens_bkg, sens_bkg_ref)
        sens_ratio = pem_utils.nonzero_divide(sens_inj_asd, sens_bkg_asd, 1)
        darm_ratio = pem_utils.nonzero_divide(darm_inj_asd, darm_bkg_asd, 1)
        above_sens = (sens_ratio > sensor_threshold)
        above_sens_scaled = (sens_ratio > scaled_sensor_threshold)
        above_darm = (darm_ratio > darm_threshold)
        measured = (freq_mask & above_sens & above_darm & above_sens_scaled)
        upperlim1 = (freq_mask & above_sens & above_darm & ~above_sens_scaled)
        upperlim2 = (freq_mask & above_sens & ~above_darm)
        belowthresh = (freq_mask & ~above_sens & ~above_darm &
                       ~above_sens_scaled &
                       np.resize(injection_type == 'broadband', nfreqs))
        nodata = ~(measured | upperlim1 | upperlim2 | belowthresh)
        # Numerator:
        # Use difference in DARM if darm & sensor threshold passed
        # Use DARM bkg otherwise
        numer = np.zeros_like(freqs)
        use_darm_diff = (measured | upperlim1)
        use_darm_inj = ~(use_darm_diff | nodata)
        numer[use_darm_diff] = np.sqrt(
            darm_inj_asd[use_darm_diff]**2 -
            darm_bkg_asd[use_darm_diff]**2)
        numer[use_darm_inj] = darm_inj_asd[use_darm_inj]
        # Denominator:
        # Use difference in sensor if sensor threshold passed
        # Use sensor bkg otherwise
        denom = np.zeros_like(freqs)
        use_sens_diff = (measured | upperlim1 | upperlim2)
        use_sens_inj = ~(use_sens_diff | nodata)
        denom[use_sens_diff] = np.sqrt(
            sens_inj_asd[use_sens_diff]**2 -
            sens_bkg_asd[use_sens_diff]**2)
        denom[use_sens_inj] = sens_inj_asd[use_sens_inj]
        factors = pem_utils.nonzero_divide(numer, denom)
        factors[nodata] = 0
        flags[measured] = 'Measured'
        flags[upperlim1 | upperlim2] = 'Upper Limit'
        flags[belowthresh] = 'Thresholds not met'
        #######################################################################
        # PEAK SEARCH
        # At each line/comb frequency, only keep the coupling factor
        # corresponding to a local maximum in the sensor injection ASD.
        if injection_type in ['comb', 'line']:
            if line_freq is not None:
                center_freqs = [line_freq]
            elif comb_freq is not None:
                center_freqs = np.arange(comb_freq, freqs.max(), comb_freq)
            else:
                center_freqs = []
            for fc in center_freqs:
                near_fc = (np.abs(freqs - fc) < peak_search_window)
                idx_near_fc = np.flatnonzero(near_fc)
                flags_near = flags[near_fc]
                asd_near = sens_inj_asd[near_fc]
                if 'Measured' in flags_near or 'Upper Limit' in flags_near:
                    if 'Measured' in flags_near:
                        flag_match = (flags_near == 'Measured')
                    else:
                        flag_match = (flags_near == 'Upper Limit')
                    idx_flag_match = idx_near_fc[flag_match]
                    idx_peak = idx_flag_match[np.argmax(asd_near[flag_match])]
                    idx_exclude = idx_near_fc[idx_near_fc != idx_peak]
                else:
                    idx_exclude = idx_near_fc
                factors[idx_exclude] = 0
                flags[idx_exclude] = 'No data'
        #######################################################################
        # OUTLIER REJECTION
        # Clean up coupling functions by demoting marginal values; only for
        # broad-band coupling functions
        elif injection_type == 'broadband':
            base_smooth = smooth_params[0]
            new_factors = np.copy(factors)
            new_flags = np.copy(flags)
            loop_range = np.arange(nfreqs)[
                (flags == 'Measured') |
                (flags == 'Upper Limit')]
            for i in loop_range:
                # Number of nearby values to compare to
                N_ = int(np.sqrt(freqs[i] * base_smooth / bin_width / 100))
                N = max([2, N_])
                lo = max([0, i - int(N / 2)])
                hi = min([nfreqs, i + N - int(N / 2) + 1])
                # Flags of nearby coupling factors
                flags_near = flags[lo:hi]
                if sum(flags[i] == flags_near) < (N / 2.):
                    # This is an outlier, demote this point to a lower flag
                    if (flags[i] == 'Measured'):
                        numer = darm_inj_asd[i]
                        denom = np.sqrt(
                            sens_inj_asd[i]**2 - sens_bkg_asd[i]**2)
                        new_factors[i] = numer / denom
                        new_flags[i] = 'Upper Limit'
                    elif (flags[i] == 'Upper Limit'):
                        new_factors[i] = darm_inj_asd[i] / sens_inj_asd[i]
                        new_flags[i] = 'Thresholds not met'
            factors = new_factors
            flags = new_flags
        cf = cls(name, freqs, factors, flags, sens_bkg_asd, darm_bkg_asd,
                 sens_inj=sens_inj_asd, darm_inj=darm_inj_asd, t_bg=t_bg,
                 t_inj=t_inj, injection_name=injection_name, unit=unit,
                 calibration=calibration, n_avg=n_avg,
                 comb_freq=comb_freq, line_freq=line_freq)
        return cf

    def crop(self, fmin, fmax):
        """
        Crop coupling function and ASDs between fmin and fmax.

        Parameters
        ----------
        fmin : float, int
            Minimum frequency (Hz).
        fmax : float, int
            Maximum frequency (Hz).
        """

        start, end = self._crop_idx(fmin, fmax)
        self.freqs = self.freqs[start:end]
        self.values = self.values[start:end]
        self.flags = self.flags[start:end]
        self.sens_bkg = self.sens_bkg[start:end]
        self.sens_inj = self.sens_inj[start:end]
        self.darm_bkg = self.darm_bkg[start:end]
        self.ambients = self.ambients[start:end]
        for attr in [
                'sens_inj', 'darm_inj', 'calibration', '_values_in_counts']:
            if hasattr(self, attr):
                val = getattr(self, attr)
                if val is not None:
                    if np.ndim(val) > 0:
                        setattr(self, attr, val[start:end])

    def to_csv(self, filename):
        """
        Save data to CSV file

        Parameters
        ----------
        filename : str
            Name of save file.
        coherence_data : dict
            Dictionary of sensor names and their corresponding coherence data
        """

        with open(filename, 'w') as file:
            # HEADER
            if self.comb_freq is not None:
                file.write('#combfreq:{:.2f}\n'.format(self.comb_freq))
            else:
                file.write('#combfreq:\n')
            if self.line_freq is not None:
                file.write('#linefreq:{:.2f}\n'.format(self.line_freq))
            else:
                file.write('#linefreq:\n')
            header = ','.join(COLUMNS)
            file.write(header + '\n')
            # DATA
            for i in range(len(self.freqs)):
                # Line of formatted data to be written to csv
                line = CSV_FORMAT \
                    % (self.freqs[i],
                       self.values[i],
                       self.values_in_counts[i],
                       self.flags[i],
                       self.sens_inj[i],
                       self.sens_bkg[i],
                       self.darm_inj[i],
                       self.darm_bkg[i])
                file.write(line + '\n')
        return
